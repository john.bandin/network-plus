# Network Plus Lab: Configuring Syslog on Raspberry Pi

## Objectives:

1. Understand the fundamentals of syslog and its importance in network management.

2. Set up a Raspberry Pi as a syslog server.

## Equipment Needed:

1. Raspberry Pi
2. Linux VM on the same network
3. Internet connection

## Background

Syslog is a widely used protocol for system management and security auditing. It provides a centralized platform for collecting and storing log messages from various network devices.

## Instructions

### Step 1: Install and Configure Syslog Server

First we will install `rsyslog` on our Raspberry Pi, our Raspberry Pi will be the syslog server. Open the terminal and type the following commands.

```
sudo apt install rsyslog
```

![](/Images/Screenshot%202023-10-29%20at%203.41.38%20PM.png)

Now using the `nano` text editor we will edit the `rsyslog` configuration file to accept remote logs. We will do this by uncommenting or adding the following lines below, to allow UDP syslog reception.

```
module(load="imudp")
input(type="imudp" port="514")
```
Using `nano` let's edit the `rsyslog.conf` file.

```
sudo nano /etc/rsyslog.conf
```

![](/Images/Screenshot%202023-10-29%20at%203.44.15%20PM.png)

Now we will restart the syslog service by using the `systemctl` command.

```
sudo systemctl restart rsyslog
```

### Step 2: Configure syslog client

Now lets open up our Kali Linux VM, install rsyslog, and configure the `rsyslog.conf` file to send all syslog messages. Once we login to our Kali linux machine open up a bash terminal.

![](/Images/Screenshot%202023-10-29%20at%203.57.29%20PM.png)

Next we will install syslog like we did in the previous steps.

```
sudo apt install rsyslog
```

![](/Images/Screenshot%202023-10-29%20at%203.59.29%20PM.png)

Next we will edit the `rsyslog.conf` file on our kali linux machine. 

```
sudo nano /etc/rsyslog.conf
```

Add the following line to send all logs to the syslog server (replace <SYSLOG_SERVER_IP> with the IP address of your syslog server Raspberry Pi):

```
*.* @<SYSLOG_SERVER_IP>:514
```

![](/Images/Screenshot%202023-10-29%20at%204.15.27%20PM.png)

Now lets restart syslog service on our Kali Linux VM using the `systemctl` command.

```
sudo systemctl restart rsyslog
```

### Step 3: Start a wireshark capture

Now lets perform a wireshark capture on `WLAN0`. Follow these instructions.

On the terminal of your raspberry Pi type `wireshark`. The wireshark applciation will appear. Highlight the `wlan0` port and click the `shark` button at the top left.

![](/Images/Screenshot%202023-10-28%20at%202.00.42%20PM.png)


### Step 4: Testing SYSLOG

Now on our Kali Linux let's try to send a log message to our Syslog server on our raspberry Pi. Run this command from the bash terminal.

```
logger "test log from $(hostname)"
```

Now back on our Raspberry lets do two things. 

1. Search in wireshark `syslog` to inspect our syslog packet. And notice the destination port is `514`

![](/Images/Screenshot%202023-10-29%20at%204.19.57%20PM.png)

2. Now lets check the log files on our raspberry Pi.

```
cat /var/log/syslog
```

![](/Images/Screenshot%202023-10-29%20at%204.21.23%20PM.png)