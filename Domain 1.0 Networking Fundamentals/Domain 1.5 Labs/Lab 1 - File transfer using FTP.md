# Network Plus Lab: Using filezilla to transfer via FTP

![](/Images/1.png)

## Objective

In this lab you will learn how to send a document via FTP using Filezilla from the Filezilla client to your Raspberry Pi FTP Server. By the end of this lab, you should be able to:

- Understand how to use filezilla 
- Gain a deeper understanding for FTP

## Lab Setup

1. A windows computers that is on the same network as the Raspberry Pi
2. Raspberry Pi connected to the same network as your workstation
3. Stable internet connection

## Instructions

### Step 1: Open filezilla

On your windows computer open up Filezilla by clicking the search bar and typing "filezilla". If you do not have Filezilla downloaded you can install it from the class share drive. See instructor for guidance.

![](/Images/Screenshot%202023-10-27%20at%209.02.12%20PM.png)

![](/Images/Screenshot%202023-10-27%20at%209.02.51%20PM.png)

### Step 2: Find the IP address of your Raspberry Pi

First we must find out the IP address of our raspberry Pi. To do this open the Raspberry Pi Terminal by clicking the "Ctrl + Alt + T" keys. Then type `ip a`. This command will show us the IP address of our `WLAN 0` interface. This is the network interface card that connects us to our Wi-Fi access point. 

![](/Images/Screenshot%202023-10-28%20at%201.43.23%20PM.png)

### Step 3: Enable SSH on your Raspberry Pi

In order to use SFTP we will need to enable SSH on our raspberry Pi. To do this we will go to `raspberry Pi preferences` --> `Raspberry Pi Configuration` --> `interfaces` --> `SSH`. We will toggle the `SSH` radio button.

![](/Images/Screenshot%202023-10-28%20at%202.04.12%20PM.png)

![](/Images/Screenshot%202023-10-28%20at%202.05.33%20PM.png)

### Step 4: Create a document using notepad.

On your windows computer create a document using notepad, and input this text.

```
This is a test file. This test file is the best test file known to man. No other test file is like me.
```

![](/Images/Screenshot%202023-10-27%20at%209.37.23%20PM.png)

![](/Images/Screenshot%202023-10-27%20at%209.39.31%20PM.png)

Now save this file to the `documents` folder and name it `file_to_send`.

![](/Images/Screenshot%202023-10-27%20at%209.40.34%20PM.png)

### Step 5: Navigate to the Documents folder in Filezilla

In filezilla navigate to the `documents` folder in the `local site`. This is your windows computer.

![](/Images/Screenshot%202023-10-27%20at%209.44.04%20PM.png)

### Step 6: Connect to the Raspberry Pi

Fill out the options on the top bar of filezilla like the photo below, ensure to replace these options with your username, password and IP address.

```
hostname == [YOUR_IP_ADDRESS]
username == [YOUR_USERNAME]
password == [YOUR_PASSWORD]
port == 22
```

![](/Images/Screenshot%202023-10-28%20at%201.05.30%20PM.png)

Now click the `quickconnect` button

### Step 7: Perform a wireshark capture

At this step we are going to perform a wireshark from our raspberry Pi. Run these commands to install wireshark on your raspberry Pi.

```
sudo apt update 
sudo apt upgrade.
sudo apt install wireshark.
sudo usermod -a -G wireshark [YOUR_USERNAME].
sudo reboot
```

![](/Images/Screenshot%202023-10-28%20at%201.45.35%20PM.png)

![](/Images/Screenshot%202023-10-28%20at%201.46.04%20PM.png)

![](/Images/Screenshot%202023-10-28%20at%201.46.49%20PM.png)

**CLICK YES**

![](/Images/Screenshot%202023-10-28%20at%201.50.31%20PM.png)

Now at the terminal of your raspberry Pi type `wireshark`. The wireshark applciation will appear. Highlight the `wlan0` port and click the `shark` button at the top left.

![](/Images/Screenshot%202023-10-28%20at%202.00.42%20PM.png)

### Step 8: transfer the file.

Now back on `filezilla` on your windows machine(local site) go to your `documents` folder, click the `file_to_send` file. On the right side of `filezilla` navigate to `desktop` folder. 

![](/Images/Screenshot%202023-10-28%20at%202.09.49%20PM.png)

Now `right-click` the `file_to_send` file and click `upload`

![](/Images/Screenshot%202023-10-28%20at%202.12.18%20PM.png)

![](/Images/Screenshot%202023-10-28%20at%202.12.18%20PM.png)

### Step 9: Check file

Now back on the Raspberry Pi check the desktop for the file.

![](/Images/Screenshot%202023-10-28%20at%202.14.50%20PM.png)

### Step 10: Search SSH on wireshark

Search `SSH` on wireshark and inspect the file transfer. Notice how it is encrypted with SSH and not plaintext like normal `FTP`

# Summary

FTP (File Transfer Protocol) and SFTP (Secure File Transfer Protocol) are both used for transferring files between computers, but they have significant differences in terms of security and functionality.

FTP (File Transfer Protocol):

**FTP (File Transfer Protocol):**
1. **Security:** FTP is not inherently secure. It transfers data in plain text, including usernames and passwords, making it vulnerable to eavesdropping and interception.
2. **Port:** FTP typically uses two ports, one for data transfer (usually port 20) and another for control (usually port 21).
3. **Authentication:** FTP uses username and password for authentication, which is sent in clear text unless used with FTPS (FTP Secure) or other encryption methods.
4. **Firewall:** FTP can have issues with firewalls and Network Address Translation (NAT) due to its use of multiple ports.
5. **Usage:** FTP is widely supported and used for basic file transfers. It is commonly used for publishing website content and sharing files.

**SFTP (Secure File Transfer Protocol):**
1. **Security:** SFTP is a secure version of FTP that encrypts both data and commands during transmission, making it resistant to eavesdropping and data breaches.
2. **Port:** SFTP typically uses a single port (usually port 22) for both data transfer and control, simplifying firewall configurations.
3. **Authentication:** SFTP relies on SSH (Secure Shell) for authentication, providing strong security through public-key or password-based authentication methods.
4. **Firewall:** SFTP is firewall-friendly because it uses a single port and does not have issues with NAT.
5. **Usage:** SFTP is commonly used for secure file transfers in business environments, data backups, and remote server management.

In summary, FTP is an older protocol that lacks inherent security, while SFTP is a modern and secure alternative that encrypts data and commands, making it the preferred choice for secure file transfers in most scenarios. If security is a concern, SFTP is recommended over FTP.



