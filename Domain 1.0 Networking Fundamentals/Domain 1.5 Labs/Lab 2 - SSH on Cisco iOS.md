# Network Plus Lab: SSH on Cisco iOS

![](/Images/2.png)

## Objective

In this lab you will configure RSA Key-pairs and enable SSH remote login capability on a Cisco iOS router. At the end of this lab you will know the commands to enable SSH on Cisco Device.

- Understand how to configure SSH on a network device
- Perform SSH on Cisco iOS 
- Perform a wireshark capture during SSH

## Lab Setup

1. EVE-NG and two emulated Cisco iOS devices
2. Internet connection
3. Alternatively you can use real networking equipment.

## Instructions

### Step 1: Open EVE-NG

Open up VMWare player, and start your EVE-NG VM. Once the VM is loaded and powered on take note of the IP address on the VM's terminal. Type that IP address in your web browsers URL using `HTTP`. Then login to your EVE-NG instance using the username and password below. Ensure you login using the `native console`. This can be selected at the login screen. 

![](/Images/Screenshot%202023-10-28%20at%2010.36.03%20PM.png)

![](/Images/Screenshot%202023-10-28%20at%2010.37.04%20PM.png)

**LOGIN INFO**
```
username: admin
password: eve
```


### Step 2: Start you EVE-NG lab

Navigate to the folder path below, and open your lab. Once the lab is open use the left navigation pane to `start all nodes`. Then console into both nodes. We do this be clicking on the routers.

```
network plus/domain 1_0/SSH Lab 1
```

![](/Images/Screenshot%202023-10-28%20at%2010.38.39%20PM.png)

![](/Images/Screenshot%202023-10-28%20at%2010.38.57%20PM.png)

![](/Images/Screenshot%202023-10-28%20at%2010.39.18%20PM.png)

![](/Images/Screenshot%202023-10-28%20at%2010.40.36%20PM.png)

![](/Images/Screenshot%202023-10-28%20at%2010.40.50%20PM.png)

### Step 3: Configure SSH on both routers

After you console into both devices we will configure SSH. To configure SSH we will need to create an RSA Public-Private key-pair. In order to generate the RSA keys we will need a FQDN (Fully Qualified Domain Name) for our device, and local credentials created on all network devices.  To configure the RSA keys we will need to configure three components on our routers. a **hostname**, **domain-name** and **credentials**.

```
router>enable
router#conf t
router(config)#hostname [YOUR_HOSTNAME]
R1(config)#username [YOUR_USERNAME] privilege 15 secret [YOUR_PASSWORD]
R1(config)#ip domain-name [YOUR_DOMAIN_NAME]
```

![](/Images/Screenshot%202023-10-28%20at%2010.45.27%20PM.png)

Now we will create our RSA Key-pair. This will need to be performed on both routers. The key size will be 1024. However, on most networks the standard is 2048, the higher/longer the key size the stronger the encryption.

```
R1(config)#crypto key generate rsa modulus 1024
```

![](/Images/Screenshot%202023-10-28%20at%2010.47.19%20PM.png)

### Step 4: Enable SSH on Cisco device

Now we will configure the router to accept SSH connectiong. Following the commands below.

```
R1#conf t
R1(config)#line vty 0 4
R1(config-line)#login local
R1(config-line)#transport input ssh
```
![](/Images/Screenshot%202023-10-29%20at%2011.44.22%20AM.png)

### Step 5: Wireshark capture of the interface

Before we test SSH we will perform a wireshark capture. `Right-click` R1 and click `capture` and then choose `eth0/0`.

![](/Images/Screenshot%202023-10-29%20at%2011.42.33%20AM.png)

![](/Images/Screenshot%202023-10-29%20at%2011.42.58%20AM.png)

![](/Images/Screenshot%202023-10-29%20at%2011.45.51%20AM.png)

### Step 6: Test SSH from R1 --> R2

From R1 we will now `SSH` to R2 using the local credentials on R2, and the R2's IP address. Use the commands below to perform an SSH remote connection from R1 --> R2

```
R1#ssh -l [USERNAME] 17.1.1.1 
```

![](/Images/Screenshot%202023-10-29%20at%2011.49.34%20AM.png)

Now with the instructor let's inspect the SSH transmissions on wireshark.

![](/Images/Screenshot%202023-10-29%20at%2011.50.32%20AM.png)

# Summary

**SSH (Secure Shell):**
- **Definition:** SSH, or Secure Shell, is a cryptographic network protocol used for securely connecting and managing network devices and servers over an unsecured network.
- **Security:** SSH provides secure communication by encrypting data and authentication information during transmission, preventing eavesdropping and unauthorized access.
- **Authentication:** SSH supports various authentication methods, including password-based authentication and public-key authentication, allowing for secure user access.
- **Port:** The default port for SSH is 22, but it can be configured to use a different port for added security.

**SSH Use in Cisco IOS (Internetwork Operating System):**
- **Purpose:** Cisco IOS is the operating system used in Cisco network devices, including routers and switches. SSH is commonly used to securely access and manage these devices.
- **Security Enhancement:** SSH is preferred over Telnet for remote management of Cisco devices due to its encryption capabilities, protecting sensitive configuration information.
- **Configuration:** To enable SSH on a Cisco device running IOS, administrators must configure SSH parameters, generate cryptographic keys, and configure user authentication methods.
- **User Access:** Once configured, SSH allows authorized users to securely access and manage Cisco devices remotely by establishing encrypted sessions.

In summary, SSH (Secure Shell) is a secure protocol used for connecting and managing network devices and servers. In Cisco IOS, SSH is employed to enhance security by enabling encrypted remote access and management of Cisco network devices.




