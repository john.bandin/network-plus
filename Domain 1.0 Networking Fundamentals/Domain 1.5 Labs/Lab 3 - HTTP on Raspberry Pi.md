# Network Plus lab: Raspberry Pi Web Server Lab

## Objective
In this lab, we will set up a basic web server on a Raspberry Pi and create a simple webpage. We will then demonstrate how a user can connect to the website using HTTP.

## Materials
- Raspberry Pi (with Raspbian OS installed)
- Computer with SSH client
- Internet connection

### Step 1: Open the terminal on your Raspberry Pi.

On your Raspberry Pi hit the `Ctrl + alt + t` keys to open up your bash terminal

![](/Images/Screenshot%202023-10-29%20at%2012.54.01%20PM.png)

### Step 2: Install a Web Server

Install a web server software on your Raspberry Pi. We'll use Apache as an example. Run the following command to install Apache:

```
sudo apt update
sudo apt install apache2
```

![](/Images/Screenshot%202023-10-29%20at%2012.55.47%20PM.png)

![](/Images/Screenshot%202023-10-29%20at%2012.56.20%20PM.png)

### Step 3: Create a simple Webpage

First we will create a directory for our website files. Run this command below to make a directory.

```
sudo mkdir ~/mywebsite
```

![](/Images/Screenshot%202023-10-29%20at%201.04.00%20PM.png)

Next we will need to make our homepage. We will use a text editor called `nano` to create an HTML file for our websites homepage. Follow the command below to create your website home page file using `nano`

```
sudo nano ~/mywebsite/index.html
```

![](/Images/Screenshot%202023-10-29%20at%201.06.27%20PM.png)

Now we will copy and paste the contents below and then save and exit from our `nano` text editor.

```html
<!DOCTYPE html>
<html>
<head>
    <title>My Raspberry Pi Webpage</title>
</head>
<body>
    <h1>Welcome to My Raspberry Pi Webpage</h1>
    <p>This is a simple webpage hosted on a Raspberry Pi.</p>
</body>
</html>
```

![](/Images/Screenshot%202023-10-29%20at%201.07.21%20PM.png)

Now hit the `Ctril + X` key to exit, `nano` will then prompt you to save. Hit the `Y` key on your keyboard, and then hit `enter` on your keyboard twice to save and exit the `nano` text editor.


### Step 4: Deploy the webpage

Now we will copy our webpages contents to the default web server directory for apache2.

```
sudo cp -r ~/mywebsite/* /var/www/html

ls -la /var/www/html
```

![](/Images/Screenshot%202023-10-29%20at%201.10.37%20PM.png)

### Step 5: Perform a wireshark capture

Before we go to our new website via our Raspberry Pi's IP address lets start a wireshark capture.

Now at the terminal of your raspberry Pi type `wireshark`. The wireshark applciation will appear. Highlight the `wlan0` port and click the `shark` button at the top left.

![](/Images/Screenshot%202023-10-28%20at%202.00.42%20PM.png)


### Step 6: navigate to the website from your PC

Now we will navigate to our website from our Windows PC. Go to the URL bar and type in the Raspberry Pis IP address using `HTTP`

![](/Images/Screenshot%202023-10-29%20at%201.18.04%20PM.png)

### Step 7: Inspect the capture

Now at wireshark search `http` and inspect the packet capture. Notice the `200` code and success.

![](/Images/Screenshot%202023-10-29%20at%201.19.22%20PM.png)


