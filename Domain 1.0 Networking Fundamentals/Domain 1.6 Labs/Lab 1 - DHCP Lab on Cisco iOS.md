# Network Plus Lab: DHCP on Cisco iOS



## Objective

In this lab you will configure DHCP on Cisco iOS and perform a wireshark capture on the DHCP process.

- Understand how to configure SSH on a network device
- Perform DHCP on Cisco iOS 
- Perform a wireshark capture during the DORA process

## Lab Setup

1. EVE-NG and one emulated Cisco iOS devices
2. Internet connection
3. Alternatively you can use real networking equipment.

## Instructions

### Step 1: Open EVE-NG

Open up VMWare player, and start your EVE-NG VM. Once the VM is loaded and powered on take note of the IP address on the VM's terminal. Type that IP address in your web browsers URL using `HTTP`. Then login to your EVE-NG instance using the username and password below. Ensure you login using the `native console`. This can be selected at the login screen. 

![](/Images/Screenshot%202023-10-28%20at%2010.36.03%20PM.png)

![](/Images/Screenshot%202023-10-28%20at%2010.37.04%20PM.png)

**LOGIN INFO**
```
username: admin
password: eve
```


### Step 2: Start you EVE-NG lab

Navigate to the folder path below, and open your lab. Once the lab is open use the left navigation pane to `start all nodes`. Then console into both nodes. We do this be clicking on the routers.

```
network plus/domain 1_0/DHCP Lab 1
```

![](/Images/Screenshot%202023-10-28%20at%2010.38.39%20PM.png)

![](/Images/Screenshot%202023-10-28%20at%2010.38.57%20PM.png)

![](/Images/Screenshot%202023-10-30%20at%208.23.57%20PM.png)

![](/Images/Screenshot%202023-10-30%20at%208.25.31%20PM.png)

![](/Images/Screenshot%202023-10-30%20at%208.26.10%20PM.png)

### Step 3: Configure DHCP on the router

For this step we will first configure the DHCP pool on our router.

```
router>enable
router#conf t
router(config)#ip dhcp pool [NAME]
```

![](/Images/Screenshot%202023-10-30%20at%208.31.55%20PM.png)

Next we will configure the default gateway, the network address, the lease time, and the DNS servers.

```
router(dhcp-config)#network 192.168.10.0 /24
router(dhcp-config)#default-router 192.168.10.1 
router(dhcp-config)#dns-servers 8.8.8.8 9.9.9.9
router(dhcp-config)#lease 7
```

![](/Images/Screenshot%202023-10-30%20at%208.37.51%20PM.png)

Next we will do some IP exclusion, and exclude a range of IP addresses from being dynamically assigned. We will block IP addresses starting at 192.168.10.50 - 192.168.10.100

```
router(config)#ip dhcp excluded-address 192.168.10.50 192.168.10.100
```

![](/Images/Screenshot%202023-10-30%20at%208.40.48%20PM.png)

### Step 4: Perform wireshark capture 

Now we will perform a wireshark capture on the router to inspect the DHCP process. `Right-click` R1 and click `capture` and then choose `eth0/0`.

![](/Images/Screenshot%202023-10-29%20at%2011.42.33%20AM.png)

![](/Images/Screenshot%202023-10-29%20at%2011.42.58%20AM.png)

![](/Images/Screenshot%202023-10-29%20at%2011.45.51%20AM.png)

### Step 5: Get an IP via DHCP

Now we will click on the VPCS to console in and set its interface to DHCP.

![](/Images/Screenshot%202023-10-30%20at%208.49.28%20PM.png)

### Step 6: Verification

now on the DHCP Server (router) let's verify the DHCP assignment with some show commands.

```
router#show ip dhcp binding
```

![](/Images/Screenshot%202023-10-30%20at%208.50.40%20PM.png)

This command will show what MAC addresses and devices are assigned an IP address.

```
router#show ip dhcp pool
```

![](/Images/Screenshot%202023-10-30%20at%208.54.04%20PM.png)

this command will show all the active pools and how many IP addresses are assigned.

### Step 7: Inspect the Wireshark capture

Now with the instructor you will inspect and discuss the DORA process.

![](/Images/Screenshot%202023-10-30%20at%208.58.16%20PM.png)

