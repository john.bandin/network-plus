# Network Plus Course: Network Time Protocol (NTP) Lab Using Raspberry Pi

## Objectives

1. Understand the basics of Network Time Protocol (NTP).
2. Set up NTP on Raspberry Pi.
3. Explore NTP commands and configurations.


## Equipment Needed:
 
1. Raspberry Pi
2. Internet connection

### Step 1: Install NTP

First we will open up a terminal on our Raspberry Pi and install NTP.

```
sudo apt install ntp
```

![](/Images/Screenshot%202023-10-29%20at%203.16.42%20PM.png)

### Step 2: Perform a wireshark capture

Now lets perform a wireshark capture on `WLAN0`. Follow these instructions.

On the terminal of your raspberry Pi type `wireshark`. The wireshark applciation will appear. Highlight the `wlan0` port and click the `shark` button at the top left.

![](/Images/Screenshot%202023-10-28%20at%202.00.42%20PM.png)

### Step 3: Configure the NTP settings

Now we will edit the NTP configuration file using the `nano` text editor. We will add these NTP servers to synchronize too.

```
server 0.north-america.pool.ntp.org
server 1.north-america.pool.ntp.org
```

```
sudo nano /etc/ntp.conf
```

![](/Images/Screenshot%202023-10-29%20at%203.23.32%20PM.png)

Now save and exit from the `nano` editor.

### Step 4: Start the NTP service

Now we will start and enable the NTP service using the `systemctl` command

```
sudo systemctl restart ntp
```

Now let's check the status of our NTP service using the `systemctl` command.

```
sudo systemctl status ntp
```

![](/Images/Screenshot%202023-10-29%20at%203.25.55%20PM.png)

### Step 5: Testing and observations

Now lets verify the NTP synchronization. Use the command below to view the NTP peers and their status.

```
ntpq -p
```

![](/Images/Screenshot%202023-10-29%20at%203.27.41%20PM.png)

